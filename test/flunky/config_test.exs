defmodule Flunky.ConfigTest do
  use ExUnit.Case, async: false

  alias Flunky.Config
  @moduletag :capture_log

  doctest Config

  setup do
    previous_env = Application.get_env(:flunky, Flunky.Config, [])
    on_exit(fn -> Application.put_env(:flunky, Flunky.Config, previous_env) end)
  end

  test "module exists" do
    assert is_list(Config.module_info())
  end

  describe "new/1" do
    @default_config [
      flunky_dispatcher: Flunky.Dispatchers.SyncDispatcher,
      flunky_error_reporter: Flunky.ErrorReporters.LogErrorReporter,
      flunky_resolver: Flunky.Resolvers.ConfigBasedResolver,
      flunky_middlewares: []
    ]

    test "should return default config with no params" do
      assert @default_config == Config.new()
    end

    test "should merge application config with no params" do
      app_config = [foo: :bar]
      Application.put_env(:flunky, Flunky.Config, app_config)
      expected = Keyword.merge(@default_config, app_config)
      assert expected == Config.new()
    end

    test "should merge passed opts" do
      app_config = [foo: :bar]
      Application.put_env(:flunky, Flunky.Config, app_config)
      opts = [bar: :baz]

      expected =
        @default_config
        |> Keyword.merge(app_config)
        |> Keyword.merge(opts)

      assert expected == Config.new(opts)
    end
  end

  describe "dispatcher/1" do
    test "should return the default dispatcher with no configured one" do
      expected = Flunky.Dispatchers.SyncDispatcher
      assert expected == Config.dispatcher()
    end

    test "should return the configured dispatcher" do
      assert __MODULE__ == Config.dispatcher(flunky_dispatcher: __MODULE__)
    end
  end

  describe "error_reporter/1" do
    test "should return the default reporter with no configured one" do
      expected = Flunky.ErrorReporters.LogErrorReporter
      assert expected == Config.error_reporter()
    end

    test "should return the configured reporter" do
      assert __MODULE__ == Config.error_reporter(flunky_error_reporter: __MODULE__)
    end
  end

  describe "resolver/1" do
    test "should return the default resolver with no configured one" do
      expected = Flunky.Resolvers.ConfigBasedResolver
      assert expected == Config.resolver()
    end

    test "should return the configured resolver" do
      assert __MODULE__ == Config.resolver(flunky_resolver: __MODULE__)
    end
  end

  describe "middlewares/1" do
    test "should return the default middlewares with no configured ones" do
      expected = []
      assert expected == Config.middlewares()
    end

    test "should return the configured middlewares" do
      assert [Flunky.Middlewares.LoggingMiddleware] ==
               Config.middlewares(flunky_middlewares: [Flunky.Middlewares.LoggingMiddleware])
    end
  end
end
