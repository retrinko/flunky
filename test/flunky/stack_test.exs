defmodule Flunky.StackTest do
  use ExUnit.Case

  alias Flunky.Stack

  @moduletag :capture_log

  doctest Stack

  test "module exists" do
    assert is_list(Stack.module_info())
  end

  describe "new/2" do
    test "should build proper stack with middlewares" do
      handler = Flunky.Samples.SayHelloHandler
      middlewares = [Flunky.Middlewares.LoggingMiddleware]

      expected_stack = [
        Flunky.Middlewares.LoggingMiddleware,
        Flunky.Samples.SayHelloHandler
      ]

      assert expected_stack == Stack.new(handler, middlewares)
    end

    test "should build proper stack with no middlewares" do
      handler = Flunky.Samples.SayHelloHandler
      assert [handler] == Stack.new(handler)
    end
  end
end
