defmodule Flunky.ErrorReporters.LogErrorReporterTest do
  use ExUnit.Case
  import ExUnit.CaptureLog
  alias Flunky.ErrorReporters.LogErrorReporter

  @moduletag :capture_log

  doctest LogErrorReporter

  test "module exists" do
    assert is_list(LogErrorReporter.module_info())
  end

  describe "report/3" do
    test "should log command name and error" do
      assert capture_log(fn ->
               LogErrorReporter.report(
                 Flunky.Command.new(:unknown_command),
                 [foo: "bar"],
                 "lorem ipsum"
               )
             end) =~ "Error handling command 'unknown_command': \"lorem ipsum\""
    end
  end
end
