defmodule Flunky.CommandTest do
  use ExUnit.Case

  alias Flunky.Command

  @moduletag :capture_log

  doctest Command

  test "module exists" do
    assert is_list(Command.module_info())
  end

  describe "new/2" do
    test "should return a well formed command" do
      command = Command.new(Flunky.Samples.SayHelloCommand, %{name: "world"})
      assert %Command{name: Flunky.Samples.SayHelloCommand, params: %{name: "world"}} == command
    end
  end

  describe "get_param/3" do
    @command Command.new(Flunky.Samples.SayHelloCommand, %{name: "world"})

    test "should return proper value when param exists" do
      assert "world" == Command.get_param(@command, :name)
    end

    test "should return default value when param does not exist" do
      assert nil == Command.get_param(@command, :non_existing_param)
      assert "default" == Command.get_param(@command, :non_existing_param, "default")
    end
  end
end
