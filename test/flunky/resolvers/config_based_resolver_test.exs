defmodule Flunky.Resolvers.ConfigBasedResolverTest do
  use ExUnit.Case

  alias Flunky.Resolvers.ConfigBasedResolver
  alias Flunky.Samples.SayHelloCommand
  alias Flunky.Samples.SayHelloHandler

  @moduletag :capture_log

  doctest ConfigBasedResolver

  test "module exists" do
    assert is_list(ConfigBasedResolver.module_info())
  end

  describe "resolve!/1" do
    test "should raise an error when no command config" do
      no_configured_command = Flunky.Command.new(:foo_command)
      assert catch_error(ConfigBasedResolver.resolve!(no_configured_command))
    end

    test "should return proper handler" do
      command = SayHelloCommand.new("world")
      expected_handler = SayHelloHandler
      assert expected_handler == ConfigBasedResolver.resolve!(command)
    end
  end
end
