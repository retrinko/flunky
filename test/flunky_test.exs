defmodule FlunkyTest do
  use ExUnit.Case
  import Mox
  # Make sure mocks are verified when the test exits
  setup :verify_on_exit!

  alias Flunky.Samples.SayHelloCommand
  alias Flunky.Samples.SayHelloHandler

  @moduletag :capture_log

  doctest Flunky

  test "module exists" do
    assert is_list(Flunky.module_info())
  end

  describe "dispatch/2" do
    @unknown_command Flunky.Command.new(:unknown_command)

    test "should return error result with unknown command" do
      assert {:error, _} = Flunky.dispatch(@unknown_command)
    end

    test "should call error reporter on errors" do
      Flunky.ErrorReporterMock
      |> expect(
        :report,
        fn @unknown_command, _, _ ->
          :ok
        end
      )

      Flunky.dispatch(@unknown_command, flunky_error_reporter: Flunky.ErrorReporterMock)
    end

    test "should return success result when handling succeeded" do
      assert {:ok, _} = Flunky.dispatch(SayHelloCommand.new("world"))
    end

    test "should return fulfilled success result when handling succeeded" do
      command = SayHelloCommand.new("world")
      {:ok, expected_data} = SayHelloHandler.handle(command)
      assert {:ok, expected_data} == Flunky.dispatch(command)
    end
  end
end
