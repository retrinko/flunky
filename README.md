# Flunky

**TODO: Add description**

## Installation

```elixir
def deps do
  [
    {:flunky, git: "https://gitlab.com/retrinko/flunky.git", tag: "0.1.0"}
  ]
end
```


## Usage

### Basic config
```elixir
# Configure dispatcher, command resolver, error reporter and middlewares  
config :flunky, Flunky.Config,
  flunky_dispatcher: Flunky.Dispatchers.SyncDispatcher,
  flunky_resolver: Flunky.Resolvers.ConfigBasedResolver,
  flunky_error_reporter: Flunky.ErrorReporters.LogErrorReporter,
  flunky_middlewares: [
    Flunky.Middlewares.LoggingMiddleware
  ]

# If your resolver is Flunky.Resolvers.ConfigBasedResolver, 
# configure a handler for each command
config :flunky, Flunky.Samples.SayHelloCommand, handler: Flunky.Samples.SayHelloHandler

```

### Run a command
```elixir
command = Flunky.Samples.SayHelloCommand.new("world")
result = Flunky.dispatch(command)
IO.inspect(result)
```

