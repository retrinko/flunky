import Config

config :flunky, Flunky.Samples.SayHelloCommand, handler: Flunky.Samples.SayHelloHandler

import_config "#{Mix.env()}.exs"
