defmodule Flunky.Resolvers.ConfigBasedResolver do
  @moduledoc "Resolves the handler for a given command based on app config."

  alias Flunky.Resolver

  @behaviour Resolver

  @impl Resolver
  def resolve!(command) do
    Keyword.fetch!(Application.fetch_env!(:flunky, command.name), :handler)
  end
end
