defmodule Flunky.Middleware do
  @moduledoc "Behaviour for middlewares."

  alias Flunky.Command
  alias Flunky.Result
  alias Flunky.Stack

  @callback init(opts :: Keyword.t()) :: Keyword.t()

  @callback handle(
              command :: Command.t(),
              stack :: Stack.t(),
              opts :: Keyword.t()
            ) :: Result.t()

  defmacro __using__(_) do
    quote do
      @impl Flunky.Middleware
      def init(opts \\ []), do: opts
      defoverridable init: 1
    end
  end
end
