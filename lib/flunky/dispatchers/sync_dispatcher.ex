defmodule Flunky.Dispatchers.SyncDispatcher do
  @moduledoc "Command dispatcher implementation."

  alias Flunky.Config
  alias Flunky.Dispatcher
  alias Flunky.Stack

  @behaviour Dispatcher

  @impl Dispatcher
  def dispatch(command, opts) do
    command
    |> handler(opts)
    |> Stack.new(middlewares(opts))
    |> Stack.next(command, opts)
  rescue
    error ->
      error_reporter(opts).report(command, opts, error)
      {:error, error}
  end

  defp handler(command, opts) do
    resolver(opts).resolve!(command)
  end

  defp error_reporter(opts) do
    Config.error_reporter(opts)
  end

  defp resolver(opts) do
    Config.resolver(opts)
  end

  defp middlewares(opts) do
    Config.middlewares(opts)
  end
end
