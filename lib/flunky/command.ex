defmodule Flunky.Command do
  @moduledoc "Models a command."

  alias Flunky.Command

  @type command_name :: atom()
  @type command_param_name :: atom()
  @type command_param_value :: any()
  @type command_params :: %{required(command_param_name) => command_param_value}
  @type t :: %__MODULE__{name: command_name, params: command_params}
  @enforce_keys [:name, :params]

  defstruct [:name, :params]

  @spec new(name :: command_name, params :: command_params) :: Command.t()
  def new(name, params \\ %{}) do
    %__MODULE__{
      name: name,
      params: params
    }
  end

  @spec get_param(
          command :: Command.t(),
          param_name :: command_param_name,
          default_value :: command_param_value
        ) :: command_param_value
  def get_param(%Command{} = command, param_name, default_value \\ nil) do
    command.params
    |> Map.get(param_name, default_value)
  end
end
