defmodule Flunky.Resolver do
  @moduledoc "Behaviour for command handler resolvers."
  alias Flunky.Command

  @callback resolve!(command :: Command.t()) :: module()
end
