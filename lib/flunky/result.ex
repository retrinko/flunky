defmodule Flunky.Result do
  @moduledoc "Type to model a command response."
  @type t :: {:ok, any()} | {:error, term()}
end
