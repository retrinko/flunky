defmodule Flunky.ErrorReporters.LogErrorReporter do
  @moduledoc "Log reported errors."

  require Logger
  alias Flunky.ErrorReporter

  @behaviour ErrorReporter

  @impl ErrorReporter
  def report(command, opts, error) do
    Logger.error("Error handling command '#{command.name}': #{inspect(error)}",
      command_name: command.name,
      command_params: inspect(command.params),
      command_opts: inspect(opts)
    )

    :ok
  end
end
