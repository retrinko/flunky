defmodule Flunky.Stack do
  @moduledoc "Stack module: Manages the execution stack."

  alias Flunky.Command
  alias Flunky.Result

  @type t :: [module()]

  @spec new(handler :: module(), middlewares :: [module()]) :: t()
  def new(handler, middlewares \\ []) do
    middlewares ++ [handler]
  end

  @spec next(
          stack :: t(),
          command :: Command.t(),
          opts :: Keyword.t()
        ) :: Result.t()
  def next(stack, command, opts)

  def next([next | []] = _stack, command, opts) do
    next.handle(command, next.init(opts))
  end

  def next([next | remaining_stack] = _stack, command, opts) do
    next.handle(command, remaining_stack, next.init(opts))
  end
end
