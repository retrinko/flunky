defmodule Flunky.Handler do
  @moduledoc "Behaviour for command handlers"

  alias Flunky.Command
  alias Flunky.Result

  @callback init(opts :: Keyword.t()) :: Keyword.t()

  @callback handle(
              command :: Command.t(),
              opts :: Keyword.t()
            ) :: Result.t()

  defmacro __using__(_) do
    quote do
      @impl Flunky.Handler
      def init(opts \\ []), do: opts
      defoverridable init: 1
    end
  end
end
