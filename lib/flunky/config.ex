defmodule Flunky.Config do
  @moduledoc false

  @default_config [
    flunky_dispatcher: Flunky.Dispatchers.SyncDispatcher,
    flunky_error_reporter: Flunky.ErrorReporters.LogErrorReporter,
    flunky_resolver: Flunky.Resolvers.ConfigBasedResolver,
    flunky_middlewares: []
  ]

  @spec new(Keyword.t()) :: Keyword.t()
  def new(opts \\ []) do
    @default_config
    |> Keyword.merge(Application.get_env(:flunky, __MODULE__, []))
    |> Keyword.merge(opts)
  end

  @spec dispatcher(Keyword.t()) :: module()
  def dispatcher(config \\ new()) do
    Keyword.get(config, :flunky_dispatcher, @default_config[:flunky_dispatcher])
  end

  @spec error_reporter(Keyword.t()) :: module()
  def error_reporter(config \\ new()) do
    Keyword.get(config, :flunky_error_reporter, @default_config[:flunky_error_reporter])
  end

  @spec resolver(Keyword.t()) :: module()
  def resolver(config \\ new()) do
    Keyword.get(config, :flunky_resolver, @default_config[:flunky_resolver])
  end

  @spec middlewares(Keyword.t()) :: [module()]
  def middlewares(config \\ new()) do
    Keyword.get(config, :flunky_middlewares, @default_config[:flunky_middlewares])
  end
end
