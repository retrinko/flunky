defmodule Flunky.Samples.SayHelloCommand do
  alias Flunky.Command

  @moduledoc false

  @spec new(name :: String.t()) :: Command.t()
  def new(name) do
    Command.new(__MODULE__, %{name: name})
  end
end
