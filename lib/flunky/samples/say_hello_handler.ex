defmodule Flunky.Samples.SayHelloHandler do
  @moduledoc false

  use Flunky.Handler
  alias Flunky.Command
  alias Flunky.Handler

  @behaviour Handler

  @impl Handler
  def init(opts) do
    default = [capitalize: true]
    Keyword.merge(default, opts)
  end

  @impl Handler
  def handle(
        %Command{name: Flunky.Samples.SayHelloCommand} = command,
        opts \\ init()
      ) do
    name = command |> Command.get_param(:name)

    case opts[:capitalize] do
      true -> {:ok, "Hello #{String.capitalize(name)}!"}
      _ -> {:ok, "Hello #{name}!"}
    end
  end
end
