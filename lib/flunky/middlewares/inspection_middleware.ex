defmodule Flunky.Middlewares.InspectionMiddleware do
  @moduledoc "Inspects the handling process. Only for local debugging purposes."

  use Flunky.Middleware
  alias Flunky.Command
  alias Flunky.Middleware
  alias Flunky.Stack

  @behaviour Middleware

  @impl Middleware
  def handle(%Command{} = command, stack, opts \\ init()) do
    prefix = __MODULE__ |> to_string() |> String.split(".") |> List.last()
    # credo:disable-for-next-line
    IO.inspect(command, label: ">>> #{prefix} - Command")
    # credo:disable-for-next-line
    IO.inspect(opts, label: ">>> #{prefix} - Opts")
    result = Stack.next(stack, command, opts)
    # credo:disable-for-next-line
    IO.inspect(result, label: ">>> #{prefix} - Result")
    result
  end
end
