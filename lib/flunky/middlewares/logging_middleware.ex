defmodule Flunky.Middlewares.LoggingMiddleware do
  @moduledoc "Logs the handling process."

  use Flunky.Middleware
  require Logger
  alias Flunky.Command
  alias Flunky.Middleware
  alias Flunky.Stack

  @behaviour Middleware

  @impl Middleware
  def init(opts) do
    default = [log_level: :debug]
    Keyword.merge(default, opts)
  end

  @impl Middleware
  def handle(%Command{} = command, stack, opts \\ init()) do
    started_at = DateTime.utc_now()
    set_logger_metadata(command, opts)
    Logger.log(opts[:log_level], "Command received: #{command.name}")
    result = Stack.next(stack, command, opts)
    handling_time = DateTime.diff(DateTime.utc_now(), started_at, :microsecond)

    Logger.log(
      opts[:log_level],
      "Command executed: #{command.name} (#{microseconds_to_string(handling_time)})",
      command_result: result,
      command_handling_time_microsecond: handling_time
    )

    reset_logger_metadata()
    result
  end

  defp set_logger_metadata(command, opts) do
    Logger.metadata(
      command_name: command.name,
      command_params: command.params,
      command_opts: opts
    )
  end

  defp reset_logger_metadata do
    Logger.metadata(
      command_name: nil,
      command_params: nil,
      command_opts: nil
    )
  end

  @spec microseconds_to_string(microseconds :: integer()) :: String.t()
  defp microseconds_to_string(microseconds)

  defp microseconds_to_string(microseconds) when microseconds > 1_000_000 do
    [(microseconds / 1_000_000) |> to_string(), "s"] |> Enum.join()
  end

  defp microseconds_to_string(microseconds) when microseconds > 1_000 do
    [(microseconds / 1_000) |> to_string(), "ms"] |> Enum.join()
  end

  defp microseconds_to_string(microseconds) do
    [microseconds |> to_string(), "µs"] |> Enum.join()
  end
end
