defmodule Flunky.Dispatcher do
  @moduledoc "Behaviour for command dispatchers."

  alias Flunky.Command
  alias Flunky.Result

  @callback dispatch(
              command :: Command.t(),
              opts :: Keyword.t()
            ) ::
              Result.t()
end
