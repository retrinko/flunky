defmodule Flunky do
  @moduledoc false

  alias Flunky.Command
  alias Flunky.Config
  alias Flunky.Result

  @spec dispatch(command :: Command.t(), opts :: Keyword.t()) :: Result.t()
  def dispatch(command, opts \\ []) do
    config = Config.new(opts)
    dispatcher = Config.dispatcher(config)
    dispatcher.dispatch(command, config)
  end
end
